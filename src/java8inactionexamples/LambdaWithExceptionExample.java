/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8inactionexamples;

import com.sun.istack.internal.logging.Logger;
import java.util.function.Supplier;

/**
 *
 * @author slorenzo
 */
public class LambdaWithExceptionExample {

    static Logger log = Logger.getLogger(PredicateExample.class);

    public static void main(String[] args) {
        Supplier<Apple> s1 = () -> new Apple();
        Supplier<Apple> s2 = Apple::new;
        
        System.out.println(s1.get());
        System.out.println(s2.get());
    }
    
    static class Apple {

        public Apple() {
        }
        
    }

//    interface FunctionWithException<T, R> {
//
//        R apply(T t) throws Exception;
//
//    }

}
