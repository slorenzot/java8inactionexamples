/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8inactionexamples;

import com.sun.istack.internal.logging.Logger;

/**
 *
 * @author slorenzo
 */
public class LambdaHelloWorld {
    
    static Logger log = Logger.getLogger(LambdaHelloWorld.class);

    public static void process(Runnable r) {
        r.run();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        process(() -> log.info("Hola Mundo!"));
    }

}
