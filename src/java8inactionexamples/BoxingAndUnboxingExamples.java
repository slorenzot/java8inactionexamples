package java8inactionexamples;

import com.sun.istack.internal.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author slorenzo
 */
public class BoxingAndUnboxingExamples {

    static Logger log = Logger.getLogger(BoxingAndUnboxingExamples.class);

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 300; i < 400; i++) {
            list.add(i);
        }

        IntPredicate evenNumbers = (int i) -> i % 2 == 0;
        System.out.println("Unboxing to int " + evenNumbers.test(1000));

        Predicate<Integer> oddNumbers = (Integer i) -> i % 2 == 0;
        System.out.println("Boxing to Integer " + oddNumbers.test(1000));

        log.info(list.toString());
    }

}
