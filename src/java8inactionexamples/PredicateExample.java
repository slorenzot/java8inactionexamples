/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8inactionexamples;

import com.sun.istack.internal.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author slorenzo
 */
public class PredicateExample {

    static Logger log = Logger.getLogger(PredicateExample.class);

    public static <T> List<T> filterColors(List<T> list, Predicate<T> p) {
        List<T> results = new ArrayList<>();
        for (T s : list) {
            if (p.test(s)) {
                results.add(s);
            }
        }
        return results;
    }

    public static void main(String[] args) {
        List<String> listOfColors
                = Arrays.asList("WHITE", "BLACK", "YELLOW", "GREEN");

        Predicate<String> containEPredicate = (String s) -> s.contains("E");
        List<String> results = filterColors(listOfColors, containEPredicate);
        
        log.info(results.toString());
    }

}
