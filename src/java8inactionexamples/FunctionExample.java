/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8inactionexamples;

import com.sun.istack.internal.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 *
 * @author slorenzo
 */
public class FunctionExample {

    static Logger log = Logger.getLogger(FunctionExample.class);

    public static <T, R> List<R> map(List<T> list, Function<T, R> f) {
        List<R> results = new ArrayList<>();

        for (T s : list) {
            results.add(f.apply(s));
        }

        return results;
    }

    public static void main(String[] args) {
        List<String> elements = Arrays.asList("lambdas", "in", "Action");
        List<Integer> results = map(elements, s -> s.length());

        log.info(results.toString());
    }

}
