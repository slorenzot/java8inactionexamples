/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8inactionexamples;

import com.sun.istack.internal.logging.Logger;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author slorenzo
 */
public class ConsumerExample {

    static Logger log = Logger.getLogger(ConsumerExample.class);

    public static <T> void forEach(List<T> list, Consumer<T> c) {
        for (T e : list) {
            c.accept(e);
        }
    }

    public static void main(String[] args) {
        List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
        
        forEach(elements, e -> log.info(String.valueOf(e)));
    }

}
